import Board
import Player

'''
Object Oriented version
of our Noughts and Crosses game
'''
Player1 = Player.Player("Bertie")
print('Hello '+Player1.name)
print(Player1.score)
Player1.send_message("Hi there!")
oxo_board = Board.TextBoard(3,3)
example_tile_data = (
  'O X',
  'XOX',
  '  X'
)
oxo_board.render(example_tile_data)

battleships_board = Board.TextBoard(10,10)
example_tile_data = (
  ' *        ',
  '   ###    ',
  '          ',
  '     #    ',
  '     #    ',
  '     #    ',
  '     #    ',
  '  *       ',
  '        ##',
  '          ',
)
battleships_board.render(example_tile_data)

def start_up():
  '''
  Do all of the initial work 
  before entering the main loop

  So:
  Create the board
  Create the players
  Find out whether each player is human or not
  Choose the tokens for the players
  Initialise the tile data
  Initialise the player flip (player1 then player2 then player1 etc.)
  '''
  pass

def main_loop():
  '''
  The main game loop

  So:
  Display (render) the board
  Ask one of the players where they want to place their token
  Validate what they entered
  If it was invalid, display a message and ask them to enter again
  If it was valid:
    update the tile data
    check whether someone won
    If nobody won:
      flip the player flip
    Else:
      end the main loop
      show the game stats.
  '''
  pass
