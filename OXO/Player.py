'''
Player class


Atributes:
score
number_of_moves
name
replay_list
tile

Methods:
set_destination
move
validate_coordinate
send_message

Subclasses:

HumanPlayer
set_destination 
  needs to enter a coordinate
  may need a game object to validate the coordinate
    using validate_coordinate

RobotPlayer
  needs to decide a coordinate
  will also need the game object validate 
    using validate_coordinate



'''

class Player:
  # constructor
  def __init__(self, name):
    '''
    width should be a positive integer
    height should be a positive integer
    '''
    self.score = 0
    self.name = name
    self.tile = ""
    self.movecounter = 0

  def send_message(self, my_message):
    print(my_message)
    
  

class TextBoard(Player):
  pass
