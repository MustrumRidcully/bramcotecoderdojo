class Board:
  # constructor
  def __init__(self, width, height):
    '''
    width should be a positive integer
    height should be a positive integer
    '''
    self.width = width
    self.height = height

class TextBoard(Board):
  #constructor
  def __init__(self, width, height):
    self.width = width
    self.height = height
    # use printf-style String Formatting
    #ten_pluses  = '++++++++++'
    #twenty_pluses = ten_pluses + ten_pluses
    #self.line_text = twenty_pluses[:(width * 2)+1]
    plus_width = (width * 2)+1
    width_pluses = '{0:{fill}{align}{width}}'.format('', fill='+', align='<', width=plus_width)

    twenty_char = ' A B C D E F G H I J'
    self.line_text = width_pluses
    self.abc_text = '  ' + twenty_char[:(width *2)+1]

  def render(self, tile_data):
    '''
    draw a text board

    tile_data will be an array of strings
    e.g.
    "X O",
    "OXX", 
    "OO "

    These should be inserted into the displayed 
    grid
    e.g.
      +++++++
    3 |X| |O|
      +++++++
    2 |O|X|X|
      +++++++
    1 |O|O| |
      +++++++
       A B C

    in this example, height = 3
    and width = 3
    '''
    for y in range(self.height):
      '''
      y is iterating through the rows
      y = 0, 1, 2 etc
      '''

      # print '+++++'
      print('   ' + self.line_text)

      '''
      we now need something like:
      3 |X| |O|

      we will refer to the '3' as 'prefix'
      prefix should be 3 on the first row if there are 3 rows of tile_data
      3 = 3 - 0
      prefix should be 2 on the second row if there are 3 rows of tile_data
      2 = 3 - 1
      '''
      #prefix = len(tile_data) - y
      #row_text = str(prefix) + ' |'
      row_text =  f'{len(tile_data) - y:2} |'

      for x in range(len(tile_data[y])):
        '''
        x is iterating through the characters 
        in the yth row of tile_data
        so e.g. x = 'X then ' ' then 'O'
        x = 0, 1, 2 etc
        '''

        '''
        we already have '3 |' for this row
        so we need to append 'X' or whatever is in the tile_data, followed by '|'
        '''
        row_text = row_text + tile_data[y][x] + '|'
      '''
      We should now have the full
      row text so print it.
      '''
      print(row_text)
    # print '+++++'
    print('  ' + self.line_text)
    # print '   A  B  C  D etc.'
    print(self.abc_text)
